import com.tdd.alfresco.FizzBuzz;
import org.junit.jupiter.api.*;

public class FizzBuzzTest {

    @BeforeEach
    void init(TestInfo testInfo) {
        System.out.println("Start..." + testInfo.getDisplayName());
    }

    @AfterEach
    void tearDown(TestInfo testInfo) {
        System.out.println("Finished..." + testInfo.getDisplayName());
    }

    @DisplayName("Test FizzBuzz computation")
    @Test
    public void testFizzBuzzComputation() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        fizzBuzz.compute(20, false);
        Assertions.assertEquals(11, fizzBuzz.getIntegerCounter());
        Assertions.assertEquals(5, fizzBuzz.getFizzCounter());
        Assertions.assertEquals(3, fizzBuzz.getBuzzCounter());
        Assertions.assertEquals(1, fizzBuzz.getFizzBuzzCounter());
    }

    @DisplayName("Test Alfresco computation - output string")
    @Test
    public void testFizzBuzzComputationString() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        fizzBuzz.compute(20, true);
        Assertions.assertEquals("1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16 17 fizz 19 buzz", fizzBuzz.getComputationString());
    }

}

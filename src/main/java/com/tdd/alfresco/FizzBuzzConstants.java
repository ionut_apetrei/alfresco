package com.tdd.alfresco;

public class FizzBuzzConstants {
    public static final String FIZZ = "fizz";
    public static final String BUZZ = "buzz";
    public static final String FIZZBUZZ = "fizzbuzz";
}

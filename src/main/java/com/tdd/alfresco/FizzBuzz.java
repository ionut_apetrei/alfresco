package com.tdd.alfresco;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
@NoArgsConstructor
public class FizzBuzz {

    private int integerCounter;
    private int fizzCounter;
    private int buzzCounter;
    private int fizzBuzzCounter;
    private String computationString;

    public void compute(int val, boolean displayOutput) {
        List<String> elements = IntStream.rangeClosed(1, val)
                .sequential()
                .mapToObj(i -> i % 15 == 0 ? FizzBuzzConstants.FIZZBUZZ
                        : (i % 3 == 0) ? FizzBuzzConstants.FIZZ
                        : i % 5 == 0 ? FizzBuzzConstants.BUZZ
                        : Integer.toString(i))
                .collect(Collectors.toList());
        updateCounters(elements);
        computationString = String.join(" ", elements).trim();
        if (displayOutput) {
            displayComputationOutput();
        }
    }

    private void updateCounters(List<String> elements) {
        if (elements != null && elements.size() != 0) {
            elements.forEach(s -> {
                if (s.matches(FizzBuzzConstants.FIZZBUZZ)) {
                    fizzBuzzCounter++;
                } else if (s.matches(FizzBuzzConstants.FIZZ)) {
                    fizzCounter++;
                } else if (s.matches(FizzBuzzConstants.BUZZ)) {
                    buzzCounter++;
                } else {
                    integerCounter++;
                }
            });
        }
    }

    private void displayComputationOutput() {
        System.out.println(this.computationString);
    }
}
